class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :name, limit: 100
      t.text :description, limit: 300
      t.string :email
      t.date :date

      t.timestamps
    end

    create_table :comments do |t|
      t.references :event
      t.string :author
      t.string :msg, limit: 140

      t.timestamps
    end
  end
end
