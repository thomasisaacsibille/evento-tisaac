# Evento
 
##  Instructions

Page "Home"

Afficher une liste des évènements avec toutes leurs caractéristiques (cf. ci-dessous)
Afficher un bouton "Ajouter un évènement" pour ouvrir un formulaire "Évènement" (cf. partie suivante)
Ajouter un lien sur le titre des évènements pour afficher la page "Évènement"
Les évènements dont la date est dans les 10 prochains jours, doivent avoir leur titre affiché en rouge  
Les évènements dont la date est déjà passée doivent avoir leur titre affiché en gris
Les évènements dont la date est déjà passée depuis plus de 10 jours ne doivent pas être affichés
2 possibilités pour trier les évènements :
    Tri par date des évènements
    Tri par organisateur et nom
Facultatif : Affichage d'un histogramme avec le nombre d'évènements par jour, allant d'il y a 10 jours à dans 10 jours (soit 21 jours en tout)

Formulaire "Évènement"

Formulaire permettant d'ajouter des évènements en base de données :

Nom de l'évènement (obligatoire, 60 caractères max)
validates_length_of :description, :maximum => 60
change_column :workorders, :description, :string,  :limit => 100
Date de l'évènement (obligatoire)
Description (300 caractères max)
validates_length_of :description, :maximum => 300
change_column :workorders, :description, :string,  :limit => 100

Email de l’organisateur (obligatoire, email valide requis)
validates :email, format: { with: URI::MailTo::EMAIL_REGEXP } 


Tous les champs doivent être valides pour pouvoir enregistrer un événement.

Ce formulaire est affiché sous forme de modal ou directement dans la page.
Page vue "Évènement" unique

Afficher toutes les caractéristiques de l'évènement
Afficher un formulaire permettant d'ajouter un commentaire sur l'évènement. 2 informations :
    Auteur (obligatoire)
    Message (obligatoire, 140 caractères max)
Afficher la liste des commentaires par ordre chronologique, du plus ancien au plus récent.

Remarques

Il sera intéressant de penser à l'UX et au look général de l'application.

Un README complété sera apprécié.
Restitution de l'exercice

Le code sur GitHub ou GitLab
L'app hébergée sur une instance gratuite de type Heroku


## Installation

### General dependencies

Need to have posgres, ruby with bundler

### Gems
To install all gems run 
```
bundle install
````

### Start project
Run
```
bundle exec rails db:create
bundle exec rails db:migrate
bundle exec rails server
```
Open your browswer with http://127.0.0.1:3000

## Deploy
Event is deployed with heroku, just have to push master branch on Heroku repo
