class Event < ApplicationRecord
  has_many :comments, -> { order :created_at }, dependent: :destroy

  validates :name, :date, :email, presence: true
  validates :email, format: { with: URI::MailTo::EMAIL_REGEXP } 

  validates_length_of :name, maximum: 60
  validates_length_of :description, maximum: 60
end
