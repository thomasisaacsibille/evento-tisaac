class Comment < ApplicationRecord
  belongs_to :event

  validates :msg, :author, presence: true 

  validates_length_of :msg, maximum: 140

end

