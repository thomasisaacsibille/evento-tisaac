class EventsController < ApplicationController
  def index
    order_params = params[:order].present? ? params[:order] : :name

    @events = Event.where(
      'date >= ?', Date.today - 10.days
    ).order(order_params)
  end

  def new
    @event = Event.new
  end

  def create
    @event = Event.new(event_params)
    if @event.save
      redirect_to events_path
    else
      render :new
    end
  end

  def show
    @event = Event.find(params[:id])
    @comment = Comment.new
  end
    

  private
  def event_params
    params.require(:event).permit(:name, :date, :description, :email)
  end
end
