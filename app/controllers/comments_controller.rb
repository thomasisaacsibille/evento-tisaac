class CommentsController < ApplicationController
  def create
    @comment = Event.find(params[:event_id]).comments.new(comment_params)
    if @comment.save
      redirect_to event_path(@comment.event)
    else
      @event = @comment.event
      render :template => "events/show"
    end
  end

  private
  def comment_params
    params.require(:comment).permit(:author, :msg)
  end
end
