module EventHelper
  def event_class_color(event)
    if event.date.past?
      'table-secondary'
    elsif Date.today + 10.days > event.date
      'table-danger'
    else
      'table-dark'
    end
  end
end
