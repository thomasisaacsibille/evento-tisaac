Rails.application.routes.draw do
  # get 'welcome/index'

  root to: 'events#index'
  resources :events do 
    resources :comments, only: [:create]
  end

end
